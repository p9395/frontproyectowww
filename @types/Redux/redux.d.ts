import { ThunkDispatch } from 'redux-thunk';

declare global {
  interface IAction {
    type: BecasActionTypes
    value?: any
    payload?: any
  }

  interface IRootState {
    becas: IBecasReducerType
    noticias: INoticiasReducerType
    paises: IPaisReducerType
    universidades: IUniversidadReducerType
  }

  type IThunkResult<R = void> = (
    dispatch: IThunkDispatch,
    getState: () => IRootState,
    extraArgument: any,
  ) => R;

  type IThunkDispatch = (obj: IAction | IThunkResult) => void

  type ThunkDispatchType = ThunkDispatch<IRootState, void, IAction>
}
