type httpVerbType = "get" | "post" | "put" | "delete";

interface IMakeRequest {
    query: string,
    httpVerb: httpVerbType,
    variables?: IObj,
    config?: AxiosRequestConfig<any>
}
