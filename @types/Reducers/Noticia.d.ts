interface INoticia {
    titulo: string
    enunciado: string
    imagen: string
    url: string
}

interface INoticiasReducerType {
    noticias: INoticia[]
}