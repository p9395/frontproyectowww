interface IBeca {
    id?: string,
    nombre: string,
    categoria: string,
    porcentaje: number,
    pais: IPais,
    universidad: IUniversidad,
    requerimientos: string,
    internacional: boolean,
    principalPage: boolean
}

interface ICrearBeca {
    nombre: string,
    categoria: string,
    porcentaje: number,
    pais_id: number,
    universidad_id: number,
    requerimientos: string,
    internacional: boolean,
    principalPage: boolean
}

interface IBecasReducerType {
    becas: IBeca[],
    becaSelected: IBeca
}