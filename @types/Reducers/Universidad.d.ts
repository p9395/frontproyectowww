interface IUniversidad {
    id: string,
    nombre: string,
    nit: number
}

interface IUniversidadReducerType {
    universidades: IUniversidad[],
}