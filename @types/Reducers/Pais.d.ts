interface IPais {
    id: string,
    nombre: string,
}

interface IPaisReducerType {
    paises: IPais[],
}