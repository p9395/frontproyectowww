import { Reducer } from 'redux';
import { BecasActionTypes } from './BecasActions';

export const initialState: IBecasReducerType = {
  becas: [],
  becaSelected: null
};

const reducer: Reducer<IBecasReducerType, IAction> = (state = initialState, action: IAction): IBecasReducerType => {
  switch (action.type) {
    case BecasActionTypes.SET_BECAS:
      return {
        ...state,
        becas: action.value,
      };
    case BecasActionTypes.SET_SELECTED_BECA:
      return {
        ...state,
        becaSelected: action.value,
      };
    default:
      return state;
  }
};

export default reducer;