import awaitToJs from 'await-to-js';
import { makeRequest } from "../../utils/client";

export enum BecasActionTypes {
  SET_BECAS = 'becas/SET_BECAS',
  SET_SELECTED_BECA = 'becas/SET_SELECTED_BECA',
}

export const obtenerBecas = (): IThunkResult => {
  return async (dispatch) => {
    const [error, response] = await awaitToJs(makeRequest({
      query: "http://127.0.0.1:8000/",
      httpVerb: "get"
    }));

    if (error) {
      throw error;
    }

    let becas = response.data ? response.data.response : []
    dispatch(set_becas(becas));
  }
}

export const set_becas = (value: IBeca[]): IAction => ({
  type: BecasActionTypes.SET_BECAS,
  value,
});

export const set_selected_beca = (value: IBeca): IAction => ({
  type: BecasActionTypes.SET_SELECTED_BECA,
  value,
});

export const guardarBecas = (beca: ICrearBeca): IThunkResult => {
  return async (dispatch) => {
    const [error, response] = await awaitToJs(makeRequest({
      query: "http://127.0.0.1:8000/",
      httpVerb: "post",
      variables: beca
    }));

    if (error) {
      throw error;
    }

    let becas = response.data ? response.data.response : []
    dispatch(set_becas(becas));
    document.location.pathname = "becas"
  }
}

export const borrarBecas = (id: string): IThunkResult => {
  return async (dispatch) => {
    const [error, response] = await awaitToJs(makeRequest({
      query: `http://127.0.0.1:8000/beca/${id}`,
      httpVerb: "delete",
    }));

    if (error) {
      throw error;
    }

    document.location.reload()
  }
}

export const actualizarBecas = (beca: IBeca): IThunkResult => {
  return async (dispatch) => {
    const [error, response] = await awaitToJs(makeRequest({
      query: `http://127.0.0.1:8000/beca/${beca.id}`,
      httpVerb: "put",
      variables: {
        nombre: beca.nombre, 
        categoria: beca.categoria, 
        porcentaje: beca.porcentaje, 
        pais_id: beca.pais.id, 
        universidad_id: beca.universidad.id, 
        requerimientos: beca.requerimientos, 
        internacional: beca.internacional, 
        principalPage: beca.principalPage, 
      }
    }));

    if (error) {
      throw error;
    }

    let becas = response.data ? response.data.response : []
    dispatch(set_becas(becas));
    document.location.pathname = "becas"
  }
}