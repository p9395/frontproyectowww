import { Reducer } from 'redux';
import { UniversidadActionTypes } from './UniversidadActions';

export const initialState: IUniversidadReducerType = {
  universidades: [],
};

const reducer: Reducer<IUniversidadReducerType, IAction> = (state = initialState, action: IAction): IUniversidadReducerType => {
  switch (action.type) {
    case UniversidadActionTypes.SET_UNIVERSIDAD:
      return {
        ...state,
        universidades: action.value,
      };
    default:
      return state;
  }
};

export default reducer;