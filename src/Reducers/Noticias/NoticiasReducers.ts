import { Reducer } from 'redux';
import { NoticiasActionTypes } from './NoticiasActions';

export const initialState: INoticiasReducerType = {
  noticias: [],
};

const reducer: Reducer<INoticiasReducerType, IAction> = (state = initialState, action: IAction): INoticiasReducerType => {
  switch (action.type) {
    case NoticiasActionTypes.SET_NOTICIAS:
      return {
        ...state,
        noticias: action.value,
      };
    default:
      return state;
  }
};

export default reducer;