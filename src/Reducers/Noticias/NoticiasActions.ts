import awaitToJs from 'await-to-js';
import { makeRequest } from "../../utils/client";

export enum NoticiasActionTypes {
  SET_NOTICIAS = 'Noticias/SET_NOTICIAS',
}

export const obtenerNoticias = (): IThunkResult => {
  return async (dispatch) => {
    const [error, response] = await awaitToJs(makeRequest({
      query: "https://api.nytimes.com/svc/mostpopular/v2/emailed/7.json?api-key=YLNYBwveXqgEt11dJ9NIbQmJCNGcnkcV",
      httpVerb: "get"
    }));

    if (error) {
      throw error;
    }

    let result = response.data ? response.data.results : []
    let noticias: INoticia[] = []

    result.map((noticia) => {
      noticias.push({
        titulo: noticia.title,
        enunciado: noticia.abstract,
        imagen: noticia.media.length == 0 ? "https://i.ibb.co/1mRCwcg/noticia.png" : noticia.media[0]["media-metadata"][2].url,
        url: noticia.url
      })
    })

    dispatch(set_Noticias(noticias));
  }
}

export const set_Noticias = (value: INoticia[]): IAction => ({
  type: NoticiasActionTypes.SET_NOTICIAS,
  value,
});