import awaitToJs from 'await-to-js';
import { makeRequest } from "../../utils/client";

export enum PaisActionTypes {
  SET_PAIS = 'pais/SET_PAIS',
}

export const obtenerPaises = (): IThunkResult => {
  return async (dispatch) => {
    const [error, response] = await awaitToJs(makeRequest({
      query: "http://127.0.0.1:8000/paises",
      httpVerb: "get"
    }));

    if (error) {
      throw error;
    }

    let paises = response.data ? response.data.response : []
    dispatch(set_pais(paises));
  }
}

export const set_pais = (value: IPais[]): IAction => ({
  type: PaisActionTypes.SET_PAIS,
  value,
});