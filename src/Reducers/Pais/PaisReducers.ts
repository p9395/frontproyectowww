import { Reducer } from 'redux';
import { PaisActionTypes } from './PaisActions';

export const initialState: IPaisReducerType = {
  paises: [],
};

const reducer: Reducer<IPaisReducerType, IAction> = (state = initialState, action: IAction): IPaisReducerType => {
  switch (action.type) {
    case PaisActionTypes.SET_PAIS:
      return {
        ...state,
        paises: action.value,
      };
    default:
      return state;
  }
};

export default reducer;