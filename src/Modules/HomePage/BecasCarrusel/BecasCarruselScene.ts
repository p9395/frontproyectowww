import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import BecasCarrusel from '.';
import { obtenerBecas } from '../../../Reducers/Becas/BecasActions';

const mapStateToProps = (state: IRootState) => {
  return {
    becas: state.becas,
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatchType) => bindActionCreators({
  obtenerBecas,
}, dispatch);

interface IOwnProps {
}

type TMapStateToProps = ReturnType<typeof mapStateToProps>;
type TMapDispatchToProps = ReturnType<typeof mapDispatchToProps>;

export type BecasCarruselProps = IOwnProps & TMapStateToProps & TMapDispatchToProps;
export default connect(mapStateToProps, mapDispatchToProps)(BecasCarrusel);
