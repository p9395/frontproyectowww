import * as React from 'react';
import { Link } from 'react-router-dom';
import { BecasCarruselProps } from './BecasCarruselScene';

class BecasCarrusel extends React.Component<BecasCarruselProps> {
  constructor(props: any) {
    super(props);
  }

  componentDidMount() {
    this.props.obtenerBecas();
  }

  construirCartas(becas: IBeca[]) {
    return (
      becas.map((beca, index) => {
        if (beca.principalPage) {
          return (
            <Link to={`beca/${beca.id}`} style={{ textDecoration: "none", color: "black" }} key={`link${index}`}>
            <div className={index == 0 ? "carousel-item active" : "carousel-item"} key={`card${index}`}>
              <div className="card">
                <svg 
                style={{
                  backgroundImage: beca.categoria == "D" ? `url(https://i.ibb.co/wSVWb9j/doctorado.png)` :
                      beca.categoria == "M" ? `url(https://i.ibb.co/jVZwXtF/maestria.png)` :
                        null,
                  backgroundSize: "100% 100%"
                }}
                className="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Image cap" preserveAspectRatio="xMidYMid slice" focusable="false">
                </svg>

                <div className="card-body">
                  <h5 className="card-title">{beca.nombre}</h5>
                  <p className="card-text">Pais: {beca.pais.nombre}</p>
                  <p className="card-text">Universidad: {beca.universidad.nombre}</p>
                </div>
              </div>
            </div>
            </Link>
          )
        }
        else return null
      })
    )
  }

  crearCarrusel() {
    let { becas } = this.props.becas;

    if(!becas){
      return
    }

    return (
      <div id="carouselExampleDark" className="carousel carousel-dark slide" data-bs-ride="carousel"
        style={{ display: "flex", alignContent: "center", justifyContent: "center" }}
      >
        <div className="carousel-inner" style={{ width: "50%" }} >
          {this.construirCartas(becas)}
        </div>
        <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>
    )
  }

  render() {
    return (
      <div style={{ margin: "20px 0" }}>
        {this.crearCarrusel()}
      </div>
    )
  }
}

export default BecasCarrusel;