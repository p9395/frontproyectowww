import * as React from 'react';
import { NoticiasCarruselProps } from './NoticiasCarruselScene';

class NoticiasCarrusel extends React.Component<NoticiasCarruselProps> {
  constructor(props: any) {
    super(props);
  }

  componentDidMount() {
    this.props.obtenerNoticias();
  }

  construirCartas(noticias: INoticia[]) {
    return (
      noticias.map((noticia, index) => {
        return (
          <div 
          onClick={()=>{ 
            return document.location.href = noticia.url
          }}
          style={{cursor: "pointer"}}
          className={index == 0 ? "carousel-item active" : "carousel-item"} key={`card${index}`}>
            <div className="card">
              <img src={noticia.imagen} />

              <div className="card-body" style={{ height: "150px" }}>
                <h5 className="card-title">{noticia.titulo}</h5>
                <p className="card-text">Pais: {noticia.enunciado}</p>
              </div>
            </div>
          </div>
        )

      })
    )
  }

  crearCarrusel() {
    let { noticias } = this.props.noticias;

    if(!noticias){
      return
    }

    return (
      <div id="carouselExampleDark1" className="carousel carousel-dark slide" data-bs-ride="carousel"
        style={{ display: "flex", alignContent: "center", justifyContent: "center" }}
      >
        <div className="carousel-inner" style={{ width: "50%" }} >
          {this.construirCartas(noticias)}
        </div>
        <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark1" data-bs-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleDark1" data-bs-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>
    )
  }

  render() {
    return (
      <div style={{ margin: "20px 0" }}>
        <div style={{
          display: "flex",
          alignContent: "center",
          justifyContent: "center"
        }}>
          <img src="https://i.ibb.co/3h6TQPd/image.png" alt="image" />
        </div>
        {this.crearCarrusel()}
      </div>
    )
  }
}

export default NoticiasCarrusel;