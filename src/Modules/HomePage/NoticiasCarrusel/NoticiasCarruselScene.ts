import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import NoticiasCarrusel from '.';
import { obtenerNoticias } from '../../../Reducers/Noticias/NoticiasActions';

const mapStateToProps = (state: IRootState) => {
  return {
    noticias: state.noticias,
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatchType) => bindActionCreators({
  obtenerNoticias,
}, dispatch);

interface IOwnProps {
}

type TMapStateToProps = ReturnType<typeof mapStateToProps>;
type TMapDispatchToProps = ReturnType<typeof mapDispatchToProps>;

export type NoticiasCarruselProps = IOwnProps & TMapStateToProps & TMapDispatchToProps;
export default connect(mapStateToProps, mapDispatchToProps)(NoticiasCarrusel);
