import * as React from 'react';
import { HomePageProps } from './HomePageScene';
import BecasCarrusel from './BecasCarrusel/BecasCarruselScene';
import NoticiasCarrusel from './NoticiasCarrusel/NoticiasCarruselScene';

class HomePage extends React.Component<HomePageProps> {
  constructor(props: any) {
    super(props);
  }

  render() {
    return (
      <div>
          <BecasCarrusel />
          <NoticiasCarrusel />
      </div>
    )
  }
}

export default HomePage;