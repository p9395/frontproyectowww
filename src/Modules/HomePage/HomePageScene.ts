import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import HomePage from '.';

const mapStateToProps = (state: IRootState) => {
  return {
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatchType) => bindActionCreators({
}, dispatch);

interface IOwnProps {
}

type TMapStateToProps = ReturnType<typeof mapStateToProps>;
type TMapDispatchToProps = ReturnType<typeof mapDispatchToProps>;

export type HomePageProps = IOwnProps & TMapStateToProps & TMapDispatchToProps;
export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
