import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CrearBeca from '.';
import {guardarBecas, actualizarBecas} from "../../Reducers/Becas/BecasActions"
import {obtenerPaises} from "../../Reducers/Pais/PaisActions"
import {obtenerUniversidades} from "../../Reducers/Universidad/UniversidadActions"

const mapStateToProps = (state: IRootState) => {
  return {
    paises: state.paises,
    universidades: state.universidades,
    becas: state.becas
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatchType) => bindActionCreators({
  obtenerPaises,
  obtenerUniversidades,
  guardarBecas,
  actualizarBecas
}, dispatch);

interface IOwnProps {
}

type TMapStateToProps = ReturnType<typeof mapStateToProps>;
type TMapDispatchToProps = ReturnType<typeof mapDispatchToProps>;

export type CrearBecaProps = IOwnProps & TMapStateToProps & TMapDispatchToProps;
export default connect(mapStateToProps, mapDispatchToProps)(CrearBeca);
