import * as React from 'react';
import { CrearBecaProps } from './CrearBecaScene';

interface IState {
  beca: ICrearBeca | IBeca
  isUpdate: boolean
}

class CrearBeca extends React.Component<CrearBecaProps, IState> {
  constructor(props: any) {
    super(props);

    this.state = {
      beca: {
        nombre: "",
        categoria: "D",
        porcentaje: 0,
        pais_id: 1,
        universidad_id: 1,
        requerimientos: "",
        internacional: false,
        principalPage: false
      },
      isUpdate: false
    };
  }

  componentDidMount() {
    this.props.obtenerPaises()
    this.props.obtenerUniversidades()

    if (document.location.pathname == "/editarBeca") {
      this.setState({
        isUpdate: true,
        beca: this.props.becas.becaSelected
      })
    }
  }

  guardarBeca(beca: ICrearBeca) {
    if (beca["nombre"] == "" && beca["requerimientos"] == "") {
      return console.log("ERROR")
    }

    this.props.guardarBecas(beca)
  }

  cambioData(campo: string, valor) {
    let beca = this.state.beca
    beca[campo] = valor.target.value
    this.setState({ beca })
  }

  cambioDataBoolean(campo: string) {
    let beca = this.state.beca
    beca[campo] = !beca[campo]
    this.setState({ beca })
  }

  crearOpcionPais() {
    let { paises } = this.props.paises

    if(!paises){
      return null
    }

    return (
      <select onChange={(e) => this.cambioData("pais_id", e)} className="form-select" aria-label="Default select example">
        {
          paises.map((pais, index) => {
            return (
              <option
                selected={index == 0 ? true : false}
                value={pais.id}>
                {pais.nombre}
              </option>
            )
          })
        }
      </select>
    )
  }

  crearOpcionUniversidad() {
    let { universidades } = this.props.universidades

    if(!universidades){
      return null
    }

    return (
      <select onChange={(e) => this.cambioData("universidad_id", e)} className="form-select" aria-label="Default select example">
        {
          universidades.map((universidad, index) => {
            return (
              <option
                selected={index == 0 ? true : false}
                value={universidad.id}>
                {universidad.nombre}
              </option>
            )
          })
        }
      </select>
    )
  }

  render() {
    let { beca, isUpdate } = this.state

    if (!beca) {
      return
    }

    return (
      <div style={{
        display: "flex",
        placeContent: "center",
        flexDirection: "column",
        alignContent: "center",
        justifyContent: "center",
        alignItems: "center",
      }}>
        <form style={{ width: "70%" }}>

          {isUpdate ? <legend>Modificar Beca</legend> : <legend>Nueva Beca</legend>}
          <div className="mb-3" style={{ display: "flex", alignItems: "center" }}>
            <label htmlFor="disabledTextInput" style={{ width: "30%" }} className="form-label">
              Nombre
            </label>
            <input
              onChange={(e) => this.cambioData("nombre", e)}
              type="text" id="disabledTextInput5" className="form-control" value={beca.nombre} />
          </div>
          <div className="mb-3" style={{ display: "flex", alignItems: "center" }}>
            <label htmlFor="disabledTextInput" style={{ width: "30%" }} className="form-label">
              Categoria
            </label>
            <select onChange={(e) => this.cambioData("categoria", e)} className="form-select" aria-label="Default select example">
              <option selected={beca.categoria == "D" ? true : false} value="D">Doctorado</option>
              <option selected={beca.categoria == "M" ? true : false} value="M">Maestria</option>
            </select>
          </div>
          <div className="mb-3" style={{ display: "flex", alignItems: "center" }}>
            <label htmlFor="disabledTextInput" style={{ width: "30%" }} className="form-label">
              Porcentaje
            </label>
            <input
              onChange={(e) => this.cambioData("porcentaje", e)}
              type="number" id="disabledTextInput1"
              className="form-control" value={beca.porcentaje} />
          </div>
          <div className="mb-3" style={{ display: "flex", alignItems: "center" }}>
            <label htmlFor="disabledTextInput" style={{ width: "30%" }} className="form-label">
              Pais
            </label>
            {this.crearOpcionPais()}
          </div>
          <div className="mb-3" style={{ display: "flex", alignItems: "center" }}>
            <label htmlFor="disabledTextInput" style={{ width: "30%" }} className="form-label">
              Universidad
            </label>
            {this.crearOpcionUniversidad()}
          </div>
          <div className="mb-3" style={{ display: "flex", alignItems: "center" }}>
            <label htmlFor="disabledTextInput" style={{ width: "30%" }} className="form-label">
              Requerimientos
            </label>
            <input
              onChange={(e) => this.cambioData("requerimientos", e)}
              type="text" id="disabledTextInput4" className="form-control" value={beca.requerimientos} />
          </div>
          <div className="form-check form-switch">
            <input onChange={(e) => this.cambioDataBoolean("internacional")} className="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault1"
              checked={
                beca.internacional ? true : false
              }
            />
            <label className="form-check-label" htmlFor="flexSwitchCheckDefault1">Internacional</label>
          </div>
          <div className="form-check form-switch">
            <input onChange={(e) => this.cambioDataBoolean("principalPage")} className="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault"
              checked={
                beca.principalPage ? true : false
              } />
            <label className="form-check-label" htmlFor="flexSwitchCheckDefault">Principal page</label>
          </div>

        </form>
        <button type="submit" className="btn btn-primary"
          onClick={() => {
            if(isUpdate){
              this.props.actualizarBecas(beca as IBeca)
            }else{
              this.guardarBeca(beca as ICrearBeca)
            }
          }
          }
        >Submit</button>
      </div>
    )
  }
}

export default CrearBeca;