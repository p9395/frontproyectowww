import { Navigate, Link, NavLink } from 'react-router-dom';
import * as React from 'react';
import { BecasProps } from './BecasScene';

class Becas extends React.Component<BecasProps> {
  constructor(props: any) {
    super(props);
  }

  componentDidMount() {
    this.props.obtenerBecas();
  }

  construirBody(becas: IBeca[]) {
    if (becas.length === 0) {
      return null
    }

    return (
      <tbody>
        {
          becas.map((beca, indexBeca) => {
            let keys = Object.keys(beca)
            let link = `/beca/${beca.id}`
            keys.shift()
            keys.pop()
            return (
              <tr key={`tr${indexBeca}`}>
                {
                  keys.map((key, indexKey) => {
                    return (
                      indexKey == 0 ?
                        <td key={`th${indexBeca}${indexKey}`} >
                          <NavLink to={link} replace style={{ color: "white" }}>
                            {beca[key]}
                          </NavLink>
                        </td>
                        :
                        <td key={`th${indexBeca}${indexKey}`} >
                          {
                            key == 'pais' || key == 'universidad' ? beca[key].nombre :
                              key == 'internacional' ? beca[key] ? "SI" : "NO" :
                                key == 'categoria' ? beca[key] == "D" ? "Doctorado" : "Maestria" :
                                  beca[key]
                          }
                        </td>
                    )
                  })
                }
                <td key={`th${indexBeca}img`} >
                  <svg 
                  style={{cursor: "pointer"}} 
                  onClick={()=>{this.props.borrarBecas(beca.id)}}
                  id="i-trash" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                    <path d="M28 6 L6 6 8 30 24 30 26 6 4 6 M16 12 L16 24 M21 12 L20 24 M11 12 L12 24 M12 6 L13 2 19 2 20 6" />
                  </svg>
                  <NavLink 
                  onClick={() => {
                    console.log("holi")
                    this.props.set_selected_beca(beca)
                  }} 
                  to={"/editarBeca"} replace style={{ color: "white", margin: "0 10px" }}>
                    Editar
                  </NavLink>
                </td>
              </tr>
            )
          })
        }
      </tbody>
    )
  }

  construirHeaders(becas: IBeca[]) {
    if (becas.length === 0) {
      return null
    }

    let beca = becas[0]
    let keys = Object.keys(beca)
    keys.shift()
    keys.pop()

    return (
      <thead>
        <tr>
          {
            keys.map((key, index) => {
              return (
                <th key={`th${index}`} scope="col">{key}</th>
              )
            })
          }
          <th key={`th00`} scope="col"> </th>
        </tr>
      </thead>
    )
  }

  render() {
    let { becas } = this.props.becas

    if(!becas){
      return
    }

    return (
      <div style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        margin: "20px 0"
      }}>
        <div style={{
          width: "100%",
          display: "flex",
          justifyContent: "flex-end",
          marginRight: "80px"
        }}>
          <Link to="/crearBecas" style={{ textDecoration: "none", color: "black" }}>
            <button type="button" className="btn btn-outline-dark">
              +
            </button>
          </Link>
        </div>
        <table
          style={{ width: "80%" }}
          id="example" className="table table-dark table-striped table-hover table-sm table-bordered">
          {this.construirHeaders(becas)}
          {this.construirBody(becas)}
        </table>
      </div>
    )
  }
}

export default Becas;