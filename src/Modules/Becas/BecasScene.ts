import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Becas from '.';
import { obtenerBecas, borrarBecas, set_selected_beca } from '../../Reducers/Becas/BecasActions';

const mapStateToProps = (state: IRootState) => {
  return {
    becas: state.becas,
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatchType) => bindActionCreators({
  obtenerBecas,
  borrarBecas,
  set_selected_beca
}, dispatch);

interface IOwnProps {
}

type TMapStateToProps = ReturnType<typeof mapStateToProps>;
type TMapDispatchToProps = ReturnType<typeof mapDispatchToProps>;

export type BecasProps = IOwnProps & TMapStateToProps & TMapDispatchToProps;
export default connect(mapStateToProps, mapDispatchToProps)(Becas);
