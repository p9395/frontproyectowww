import * as React from 'react';
import { BecaProps } from './BecaScene';

interface IState {
  beca: IBeca
}

class Beca extends React.Component<BecaProps, IState> {
  constructor(props: any) {
    super(props);

    this.state = {
      beca: null
    };
  }

  componentDidMount() {
    let idSelected = document.location.pathname.split("/")[2]

    let { becas } = this.props.becas;
    becas.map((beca) => {
      if (beca.id == idSelected) {
        return this.setState({ beca })
      }
    })
  }

  render() {
    let beca = this.state.beca

    if (beca == null) {
      return
    }

    return (
      <div style={{ display: "flex", justifyContent: "center", alignContent: "center" }}>
        <form style={{ width: "70%" }}>
          <fieldset disabled>
            <legend>Beca: {beca.nombre}</legend>
            <div className="mb-3" style={{ display: "flex", alignItems: "center" }}>
              <label htmlFor="disabledTextInput" style={{ width: "30%" }} className="form-label">
                Categoria
              </label>
              <input type="text" id="disabledTextInput" className="form-control" value={beca.categoria == "D" ? "Doctorado" : "Maestria"} />
            </div>
            <div className="mb-3" style={{ display: "flex", alignItems: "center" }}>
              <label htmlFor="disabledTextInput" style={{ width: "30%" }} className="form-label">
                Porcentaje
              </label>
              <input type="text" id="disabledTextInput1" className="form-control" value={beca.porcentaje} />
            </div>
            <div className="mb-3" style={{ display: "flex", alignItems: "center" }}>
              <label htmlFor="disabledTextInput" style={{ width: "30%" }} className="form-label">
                Pais
              </label>
              <input type="text" id="disabledTextInput2" className="form-control" value={beca.pais.nombre} />
            </div>
            <div className="mb-3" style={{ display: "flex", alignItems: "center" }}>
              <label htmlFor="disabledTextInput" style={{ width: "30%" }} className="form-label">
                Universidad
              </label>
              <input type="text" id="disabledTextInput3" className="form-control" value={beca.universidad.nombre} />
            </div>
            <div className="mb-3" style={{ display: "flex", alignItems: "center" }}>
              <label htmlFor="disabledTextInput" style={{ width: "30%" }} className="form-label">
                Requerimientos
              </label>
              <input type="text" id="disabledTextInput4" className="form-control" value={beca.requerimientos} />
            </div>
            <div className="mb-3" style={{ display: "flex", alignItems: "center" }}>
              <label htmlFor="disabledTextInput" style={{ width: "30%" }} className="form-label">
                internacional
              </label>
              <input type="text" id="disabledTextInput5" className="form-control" value={beca.internacional ? "SI" : "NO"} />
            </div>
            <div className="mb-3" style={{ display: "flex", alignItems: "center" }}>
              <label htmlFor="disabledTextInput" style={{ width: "30%" }} className="form-label">
                Pagina Principal
              </label>
              <input type="text" id="disabledTextInput6" className="form-control" value={beca.principalPage ? "SI" : "NO"} />
            </div>
          </fieldset>
        </form>
      </div>
    )
  }
}

export default Beca;