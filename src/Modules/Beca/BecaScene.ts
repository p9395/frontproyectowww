import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Beca from '.';

const mapStateToProps = (state: IRootState) => {
  return {
    becas: state.becas,
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatchType) => bindActionCreators({
}, dispatch);

interface IOwnProps {
}

type TMapStateToProps = ReturnType<typeof mapStateToProps>;
type TMapDispatchToProps = ReturnType<typeof mapDispatchToProps>;

export type BecaProps = IOwnProps & TMapStateToProps & TMapDispatchToProps;
export default connect(mapStateToProps, mapDispatchToProps)(Beca);
