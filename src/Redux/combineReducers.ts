import { combineReducers } from 'redux';

import becas from '../Reducers/Becas/BecasReducers';
import noticias from '../Reducers/Noticias/NoticiasReducers';
import paises from '../Reducers/Pais/PaisReducers';
import universidades from '../Reducers/Universidad/UniversidadReducers';

export default combineReducers<IRootState>({
  becas,
  noticias,
  paises,
  universidades
});
