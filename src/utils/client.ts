import axios from 'axios';
import awaitToJs from 'await-to-js';

export const makeRequest = async (value: IMakeRequest) => {
    switch (value.httpVerb) {
        case ("get"): {
            const [error, response] = await awaitToJs(axios.get(
                value.query,
                value.config ? value.config : null
            ));
            if (error) {
                throw error;
            }
            return response;
        };
        case ("post"): {
            const [error, response] = await awaitToJs(axios.post(
                value.query,
                value.variables ? value.variables : null,
                value.config ? value.config : null
            ));
            if (error) {
                throw error;
            }
            return response;
        };
        case ("put"): {
            const [error, response] = await awaitToJs(axios.put(
                value.query,
                value.variables ? value.variables : null,
                value.config ? value.config : null
            ));
            if (error) {
                throw error;
            }
            return response;
        };
        case ("delete"): {
            const [error, response] = await awaitToJs(axios.delete(
                value.query,
                value.config ? value.config : null
            ));
            if (error) {
                throw error;
            }
            return response;
        }
    }
}