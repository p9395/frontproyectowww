import * as React from 'react';
import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux';
import { configureStore } from './Redux/setupStore';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import routes from './Routes'

const store = configureStore();

const container = document.getElementById('root');
const root = createRoot(container!);

const pestañas = [
  { name: "Home", path: "/" },
  { name: "Becas", path: "/becas" }
]

let NavBar = () => {

  let path = document.location.pathname

  return (
    <nav>
      <div style={{background: "aliceblue"}} className="nav nav-tabs" id="nav-tab" role="tablist">
        {
          pestañas.map((link, index) => {
            return (
              <Link to={link.path} style={{ textDecoration: "none" }} key={`link${index}`}>
                <button
                  className={link.path == path ? "nav-link active" : "nav-link"}
                  id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">
                  {link.name}
                </button>
              </Link>
            )
          })
        }
      </div>
    </nav>
  )
}

root.render(
  <Provider store={store as any}>
    <Router>
      <div>
        <NavBar />
        {routes()}
      </div>
    </Router>
  </Provider>
);
