import React from "react";
import { Routes, Route } from "react-router-dom";

import Becas from '../Modules/Becas/BecasScene';
import HomePage from '../Modules/HomePage/HomePageScene';
import Beca from '../Modules/Beca/BecaScene';
import CrearBeca from '../Modules/CrearBeca/CrearBecaScene';

export default function App() {
  return (
    <Routes>
      <Route path="/" element={<HomePage />} />
      <Route path="/becas" element={<Becas />} />
      <Route path="/beca/:id" element={<Beca />} />
      <Route path="/crearBecas" element={<CrearBeca />} />
      <Route path="/editarBeca" element={<CrearBeca />} />
    </Routes>
  );
}