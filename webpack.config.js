const path = require('path');

const sourcePath = path.join(__dirname, './src');
const outPath = path.join(__dirname, './dist');

module.exports = {
    context: sourcePath,
    entry: {
        main: './index.tsx'
    },
    output: {
        path: outPath,
        publicPath: '/',
        filename: '[name].bundle.js'
    },

    resolve: {
        extensions: ['.tsx', '.ts', '.js', '.json', '.jsx'],
    },

    mode: process.env.NODE_ENV || 'development',

    module: {
        rules: [{
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            }
        ],
    },

    devServer: {
        static: {
            directory: path.join(__dirname, 'src')
        },
        port: 3002,
        open: true,
        historyApiFallback: true,
    }
};